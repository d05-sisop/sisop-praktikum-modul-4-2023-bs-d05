#define FUSE_USE_VERSION 31

#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <curl/curl.h>
#include <zip.h>

#define MAX_FILENAME_LEN 256
#define MAX_URL_LEN 256
#define MAX_USERS 100
#define MAX_USERNAME_LENGTH 50
#define MAX_PASSWORD_LENGTH 50

static const char *secret_folder = "/usr/share/rahasia";
static const char *zip_file = "/usr/share/rahasia/rahasia.zip";
static const char *download_url = "https://drive.google.com/file/d/18YCFdG658SALaboVJUHQIqeamcfNY39a/view?usp=sharing"; // Ganti dengan URL unduhan yang sesuai

static size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    return fwrite(ptr, size, nmemb, stream);
}

static int download_zip()
{
    FILE *zip_fp = fopen(zip_file, "wb");
    if (!zip_fp) {
        printf("Failed to open zip file for writing\n");
        return -1;
    }

    CURL *curl = curl_easy_init();
    if (!curl) {
        printf("Failed to initialize libcurl\n");
        fclose(zip_fp);
        return -1;
    }

    curl_easy_setopt(curl, CURLOPT_URL, download_url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, zip_fp);

    CURLcode res = curl_easy_perform(curl);
    if (res != CURLE_OK) {
        printf("Failed to download zip file: %s\n", curl_easy_strerror(res));
        curl_easy_cleanup(curl);
        fclose(zip_fp);
        return -1;
    }

    curl_easy_cleanup(curl);
    fclose(zip_fp);

    return 0;
}

static int unzip_file(const char *file, const char *destination)
{
    int err = 0;

    int status;
    zip *zip_file = zip_open(file, 0, &status);
    if (!zip_file) {
        printf("Failed to open zip file: %s\n", zip_strerror(zip_file));
        return -1;
    }

    zip_int64_t num_entries = zip_get_num_entries(zip_file, 0);
    if (num_entries == -1) {
        printf("Failed to get number of entries in zip file: %s\n", zip_strerror(zip_file));
        zip_close(zip_file);
        return -1;
    }

    for (zip_int64_t i = 0; i < num_entries; i++) {
        struct zip_stat entry_stat;
        if (zip_stat_index(zip_file, i, 0, &entry_stat) == -1) {
            printf("Failed to get stat for entry %lld: %s\n", (long long)i, zip_strerror(zip_file));
            err = -1;
            continue;
        }

        if (zip_file_extract(zip_file, i, destination, ZIP_FL_OVERWRITE) == -1) {
            printf("Failed to extract entry %lld: %s\n", (long long)i, zip_strerror(zip_file));
            err = -1;
        }
    }

    zip_close(zip_file);

    return err;
}

static int rahasia_getattr(const char *path, struct stat *stbuf)
{
    int res = 0;

    memset(stbuf, 0, sizeof(struct stat));
    if (strcmp(path, "/") == 0) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
    } else if (strcmp(path, secret_folder) == 0){

struct stat real_stbuf;
if (stat(zip_file, &real_stbuf) == -1)
return -errno;
stbuf->st_mode = real_stbuf.st_mode;
stbuf->st_nlink = real_stbuf.st_nlink;
stbuf->st_size = real_stbuf.st_size;
stbuf->st_atime = real_stbuf.st_atime;
stbuf->st_mtime = real_stbuf.st_mtime;
stbuf->st_ctime = real_stbuf.st_ctime;
} else {
res = -ENOENT;
}
return res;
}

static int rahasia_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
(void) offset;
(void) fi;if (strcmp(path, "/") != 0)
    return -ENOENT;

filler(buf, ".", NULL, 0);
filler(buf, "..", NULL, 0);
filler(buf, secret_folder + 1, NULL, 0);

return 0;
}

static int rahasia_open(const char *path, struct fuse_file_info *fi)
{
if (strcmp(path, secret_folder) != 0)
return -ENOENT;
if ((fi->flags & 3) != O_RDONLY)
    return -EACCES;

return 0;
}

static int rahasia_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
size_t len;
(void) fi;
if(strcmp(path, secret_folder) != 0)
    return -ENOENT;

FILE *file = fopen(zip_file, "r");
if (file == NULL)
    return -errno;

if (fseek(file, offset, SEEK_SET) == -1) {
    fclose(file);
    return -errno;
}

len = fread(buf, 1, size, file);
if (len == -1)
    len = -errno;

fclose(file);

return len;
}

static struct fuse_operations rahasia_oper = {
.getattr = rahasia_getattr,
.readdir = rahasia_readdir,
.open = rahasia_open,
.read = rahasia_read,
};

int main(int argc, char *argv[])
{
umask(0);
// Unduh file zip
if (download_zip() != 0) {
    printf("Failed to download zip file\n");
    return -1;
}

// Unzip file zip
if (unzip_file(zip_file, secret_folder) != 0) {
    printf("Failed to unzip file\n");
    return -1;
}

return fuse_main(argc, argv, &rahasia_oper, NULL);
}



struct User {
    char username[MAX_USERNAME_LENGTH];
    char password[MAX_PASSWORD_LENGTH];
};

struct User users[MAX_USERS];
int userCount = 0;

void registerUser() {
    char username[MAX_USERNAME_LENGTH];
    char password[MAX_PASSWORD_LENGTH];
    char md5Password[MD5_DIGEST_LENGTH * 2 + 1];

    printf("Masukkan username: ");
    scanf("%s", username);

    // Periksa apakah username sudah terdaftar
    for (int i = 0; i < userCount; i++) {
        if (strcmp(users[i].username, username) == 0) {
            printf("Username sudah terdaftar. Silakan coba dengan username lain.\n");
            return;
        }
    }

    printf("Masukkan password: ");
    scanf("%s", password);

    // Hashing password dengan MD5
    unsigned char hash[MD5_DIGEST_LENGTH];
    MD5((unsigned char*)password, strlen(password), hash);

    // Konversi hash menjadi string
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
        sprintf(&md5Password[i * 2], "%02x", (unsigned int)hash[i]);
    }

    // Tambahkan user baru ke dalam array
    strcpy(users[userCount].username, username);
    strcpy(users[userCount].password, md5Password);
    userCount++;

    printf("Registrasi berhasil!\n");
}

void loginUser() {
    char username[MAX_USERNAME_LENGTH];
    char password[MAX_PASSWORD_LENGTH];
    char md5Password[MD5_DIGEST_LENGTH * 2 + 1];

    printf("Masukkan username: ");
    scanf("%s", username);

    printf("Masukkan password: ");
    scanf("%s", password);

    // Hashing password dengan MD5
    unsigned char hash[MD5_DIGEST_LENGTH];
    MD5((unsigned char*)password, strlen(password), hash);

    // Konversi hash menjadi string
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
        sprintf(&md5Password[i * 2], "%02x", (unsigned int)hash[i]);
    }

    // Mencocokkan kredensial
    for (int i = 0; i < userCount; i++) {
        if (strcmp(users[i].username, username) == 0 && strcmp(users[i].password, md5Password) == 0) {
            printf("Login berhasil!\n");
            return;
        }
    }

    printf("Login gagal. Username atau password salah.\n");
}

int main() {
    int choice;

    while (1) {
        printf("Menu:\n");
        printf("1. Register\n");
        printf("2. Login\n");
        printf("3. Exit\n");
        printf("Pilihan Anda: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                registerUser();
                break;
            case 2:
                loginUser();
                break;
            case 3:
                exit(0);
            default:
                printf("Pilihan tidak valid.\n");
        }
    }

    return 0;
}
