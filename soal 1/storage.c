#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LENGTH 1000

void readCSV(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return;
    }

    char line[MAX_LINE_LENGTH];
    char *token;
    const char *delimiter = ",";
    int nameIndex = -1, ageIndex = -1, nationalityIndex = -1, clubIndex = -1, potentialIndex = -1, photoIndex = -1;

    if (fgets(line, MAX_LINE_LENGTH, file) != NULL) {
        // mencari indeks kolom 
        token = strtok(line, delimiter);
        int index = 0;
        while (token != NULL) {
            if (strcmp(token, "Name") == 0) {
                nameIndex = index;
            } else if (strcmp(token, "Age") == 0) {
                ageIndex = index;
            } else if (strcmp(token, "Club") == 0) {
                clubIndex = index;
            } else if (strcmp(token, "Nationality") == 0) {
                nationalityIndex = index;
            } else if (strcmp(token, "Potential") == 0) {
                potentialIndex = index;
            } else if (strcmp(token, "Photo") == 0) {
                photoIndex = index;
            }
            token = strtok(NULL, delimiter);
            index++;
        }

        if (nameIndex == -1 || ageIndex == -1 || nationalityIndex == -1 || clubIndex == -1 || potentialIndex == -1 || photoIndex == -1) {
            printf("Data not found in file: %s\n", filename);
            fclose(file);
            return;
        }
    }

    // mengeprint
    printf("Name\tAge\tClub\tNationality\tPotential\tPhoto\n");
    while (fgets(line, MAX_LINE_LENGTH, file) != NULL) {
        token = strtok(line, delimiter);
        int index = 0;
        char *name = NULL, *age = NULL, *nationality = NULL, *club = NULL, *potential = NULL, *photo = NULL;
        while (token != NULL) {
            if (index == nameIndex) {
                name = token;
            } else if (index == ageIndex) {
                age = token;
            } else if (index == clubIndex) {
                club = token;
            } else if (index == nationalityIndex) {
                nationality = token;
            } else if (index == potentialIndex) {
                potential = token;
            } else if (index == photoIndex) {
                photo = token;
            }

            token = strtok(NULL, delimiter);
            index++;
        }
        if (age != NULL && atoi(age) < 25 && atoi(potential) > 85 && strcmp(club, "Manchester City") != 0) {
            printf("%s\t%s\t%s\t%s\t%s\t%s\n", name, age, club, nationality, potential, photo);
        }
    }

    fclose(file);
}

int downloadDataset() {
    char command[] = "kaggle datasets download -d bryanb/fifa-player-stats-database";
    return system(command);
}

int main() {

    // download dataset
    int downloadStatus = downloadDataset();
    if (downloadStatus != 0) {
        printf("Failed to download the dataset from Kaggle.\n");
        return 1;
    }

    // mengekstrak dataset
    printf("Extracting the dataset...\n");

    char extractCommand[] = "unzip fifa-player-stats-database.zip";
    int extractStatus = system(extractCommand);
    if (extractStatus != 0) {
        printf("Failed to extract the dataset.\n");
        return 1;
    }

    const char *filename = "FIFA23_official_data.csv";
    readCSV(filename);

    return 0;
}
