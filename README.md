# sisop-praktikum-modul-4-2023-BS-D05

## SOAL 1
### Penjelasan
Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

a. Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset. 

`kaggle datasets download -d bryanb/fifa-player-stats-database`

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 

b. Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama **FIFA23_official_data.csv** dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

c. Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.


d. Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

e. Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.


### Penyelesaian
1. Mendownload dataset dari Kaggle menggunakan command 
```
kaggle datasets download -d bryanb/fifa-player-stats-database
```
Sebelum itu perlu menginstall beberapa packages yang dibutuhkan antara lain `python3`, `python3-pip`, dan `kaggle`. Setelah itu masuk ke akun Kaggle untuk membuat token baru yang nantinya akan otomatis terdownload ke local computer dalam bentuk file `kaggle.json`. Selanjutnya, file tersebut perlu dipindahkan ke folder `./kaggle` menggunakan command
```
mv ~/Downloads/kaggle.json ~/.kaggle/
```

2. Membuat file `storage.c` yang berisi program untuk menganalisis dataset pada file **FIFA23_official_data.csv** yang akan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. 

- Pertama perlu fungsi untuk mendownload dataset menggunakan command API pada kaggle
```
int downloadDataset() {
    char command[] = "kaggle datasets download -d bryanb/fifa-player-stats-database";
    return system(command);
}

```
File yang terdownload adalah file zip yang perlu di-unzip terlebih dahulu.
```
    char extractCommand[] = "unzip fifa-player-stats-database.zip";
    int extractStatus = system(extractCommand);
    if (extractStatus != 0) {
        printf("Failed to extract the dataset.\n");
        return 1;
    }

```
- Memilih file bernama **FIFA23_official_data.csv** untuk dianalisis.
```
    const char *filename = "FIFA23_official_data.csv";
    readCSV(filename);

```
Selanjutnya, isi dari file akan dipecah menjadi beberapa token berdasarkan kolom **Name**, **Age**, **Club**, **Nationality**, **Potential**, dan **Photo**

```
int nameIndex = -1, ageIndex = -1, nationalityIndex = -1, clubIndex = -1, potentialIndex = -1, photoIndex = -1;

    if (fgets(line, MAX_LINE_LENGTH, file) != NULL) {
        // mencari indeks kolom 
        token = strtok(line, delimiter);
        int index = 0;
        while (token != NULL) {
            if (strcmp(token, "Name") == 0) {
                nameIndex = index;
            } else if (strcmp(token, "Age") == 0) {
                ageIndex = index;
            } else if (strcmp(token, "Club") == 0) {
                clubIndex = index;
            } else if (strcmp(token, "Nationality") == 0) {
                nationalityIndex = index;
            } else if (strcmp(token, "Potential") == 0) {
                potentialIndex = index;
            } else if (strcmp(token, "Photo") == 0) {
                photoIndex = index;
            }
            token = strtok(NULL, delimiter);
            index++;
        }

```
Jika data tidak ditemukan akan mencetak `Data not found in file FIFA23_official_data.csv` 
```
        if (nameIndex == -1 || ageIndex == -1 || nationalityIndex == -1 || clubIndex == -1 || potentialIndex == -1 || photoIndex == -1) {
            printf("Data not found in file: %s\n", filename);
            fclose(file);
            return;
        }

```
- Selanjutnya akan mencetak data pemain untuk data yang telah ditemukan sebelumnya
```
 while (fgets(line, MAX_LINE_LENGTH, file) != NULL) {
        token = strtok(line, delimiter);
        int index = 0;
        char *name = NULL, *age = NULL, *nationality = NULL, *club = NULL, *potential = NULL, *photo = NULL;
        while (token != NULL) {
            if (index == nameIndex) {
                name = token;
            } else if (index == ageIndex) {
                age = token;
            } else if (index == clubIndex) {
                club = token;
            } else if (index == nationalityIndex) {
                nationality = token;
            } else if (index == potentialIndex) {
                potential = token;
            } else if (index == photoIndex) {
                photo = token;
            }

            token = strtok(NULL, delimiter);
            index++;
        }
```
Serta menambahkan kondisi dimana umur harus kurang dari 25, potensial lebih dari 85, dan berasal dari club selain Manchester City.
```
if (age != NULL && atoi(age) < 25 && atoi(potential) > 85 && strcmp(club, "Manchester City") != 0) {
            printf("%s\t%s\t%s\t%s\t%s\t%s\n", name, age, club, nationality, potential, photo);
        }
```
3. Membuat Dockerfile untuk setup environment.
- Pertama, perlu menginstall beberapa package untuk menjalankan docker ini.
```
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
- Selanjutnya membuat file bernama Dockerfile yang akan menjalankan sistem ini.

Di dalam Dockerfile perlu menginstall package untuk sistem ini.
```
RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    unzip \
    && pip3 install kaggle

```
Lalu menyalin file `storage.c` ke dalam docker untuk di-compile dan dijalankan di dalam docker.
```
COPY storage.c /app/storage.c

# compile storage.c
RUN gcc -o storage storage.c

# run storage.c
CMD ["./storage"]
```
Dan juga membuat setup untuk Kaggle.
```
RUN mkdir -p ~/.kaggle \
    && echo '{"username":"parkara","key":"27beb36673b7e5ec0faa1a744bcf6860"}' > ~/.kaggle/kaggle.json \
    && chmod 600 ~/.kaggle/kaggle.json

```
4. Membuat docker image dan docker tag.

- Setelah selesai membuat Dockerfile, selanjutnya mem-build menjadi image dengan nama `storage-app`dengan command
```
sudo docker build -t storage-app .
```
Berikut adalah ketika build image storage-app
![](image/build_image.png)

- Setelah itu membuat tag bernama `YBBA` dan `YangBiruBiruAja` dengan command
```
sudo docker tag storage-app frhdhf/storage-app:YBBA
sudo docker tag storage-app frhdhf/storage-app:YangBiruBiruAja
```
Berikut adalah ketika membuat tag
![](image/docker_tag.png)

- Setelah membuat tag, selanjutnya akan di-push ke dalam akun docker kita dengan command
```
sudo docker push frhdhf/storage-app:YBBA
sudo docker push frhdhf/storage-app:YangBiruBiruAja
```
Berikut adalah ketika push tag
![](image/push_tag.png)

5. Melakukan docker compose di folder `Barcelona` dan `Napoli` dengan instance sebanyak 5 kali per folder dan pada port yang berbeda.

- Pertama, perlu membuat folder Barcelona dan Napoli dengan command
```
mkdir Barcelona
mkdir Napoli
```
- Membuat file docker-compose.yml 
```
version: "3"
services:
  storage-app:
    image: frhdhf/storage-app:YBBA
    ports:
      - "8080-8089:80"
    deploy:
      replicas: 5
```
Instance sebanyak 5 kali per folder akan membutuhkan port dengan total sebanyak 10.

- Melakukan docker compose pada folder Barcelona dan Napoli dengan command
```
sudo docker compose logs storage-app
```
Berikut adalah docker compose pada folder Barcelona
![](image/barcelona.png)

dan berikut adalah docker compose pada folder Napoli
![](image/napoli.png)

6. Berikut adalah hasil yang dapat dilihat pada link [HubDocker](https://hub.docker.com/r/frhdhf/storage-app)

![](image/docker_hub.png)

## SOAL 4

### Penjelasan

Pada suatu masa, terdapat sebuah perusahaan bernama Bank Sabar Indonesia yang berada pada masa kejayaannya. Bank tersebut memiliki banyak sekali kegiatan-kegiatan yang  krusial, seperti mengolah data nasabah yang mana berhubungan dengan uang. Suatu ketika, karena susahnya maintenance, petinggi bank ini menyewa seseorang yang mampu mengorganisir file-file yang ada di Bank Sabar Indonesia. 
Salah satu karyawan di bank tersebut merekomendasikan Bagong sebagai seseorang yang mampu menyelesaikan permasalahan tersebut. Bagong memikirkan cara terbaik untuk mengorganisir data-data nasabah dengan cara membagi file-file yang ada dalam bentuk modular, yaitu membagi file yang mulanya berukuran besar menjadi file-file chunk yang berukuran kecil. Hal ini bertujuan agar saat terjadi error, Bagong dapat mudah mendeteksinya. Selain dari itu, agar Bagong mengetahui setiap kegiatan yang ada di filesystem, Bagong membuat sebuah sistem log untuk mempermudah monitoring kegiatan di filesystem yang mana, nantinya setiap kegiatan yang terjadi akan dicatat di sebuah file log dengan ketentuan sebagai berikut. 

- Log akan dibagi menjadi beberapa level, yaitu REPORT dan FLAG.
- Pada level log FLAG, log akan mencatat setiap kali terjadi system call rmdir (untuk menghapus direktori) dan unlink (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log REPORT.
- Format untuk logging yaitu sebagai berikut.
[LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]

  Contoh:  
  REPORT::230419-12:29:28::RENAME::/bsi23/bagong.jpg::/bsi23/bagong.jpeg  
  REPORT::230419-12:29:33::CREATE::/bsi23/bagong.jpg  
  FLAG::230419-12:29:33::RMDIR::/bsi23  

a. Pada filesystem tersebut, jika Bagong membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular.  

b. Bagong menginginkan agar saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.  

c. Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.  

d. Saat Bagong melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.  
Contoh:  
file File_Bagong.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Bagong.txt.000, File_Bagong.txt.001, dan File_Bagong.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).  

e. Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).  

### Penyelesaian

#### Membaca Nama File

Pada sistem FUSE yang diminta semua file dalam direktori yang memiliki nama FUSE beserta seluruh sub direktori didalamnya akan diubah menjadi modular untuk menyelesaikan ini kami memanfaatkan fungsi strcmp untuk membandingkan path keseluruhan direktori dengan kode "module_". Jika sebuah path mengandung kode "module_" artinya direktori tersebut adalah direktori "module_" atau terlatak didalam direktori "module_".

```c
char kode[10] = "module_";

...

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){ //baca directory
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }

...

	while ((dir = readdir(dp)) != NULL) { //buat loop yang ada di dalem directory
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = dir->d_ino;
		st.st_mode = dir->d_type << 12;
		if(enc2 != NULL){
			encrypt(dir->d_name, enc2);
        } else {
			char *extension = strrchr(dir->d_name, '.');
            if (extension != NULL && strcmp(extension, ".001") == 0) {
                *extension = '\0';
                decode(newPath, dir->d_name);
            }
		}
		res = (filler(buf, dir->d_name, &st, 0));
		if(res!=0) break;
	}

	closedir(dp);
	return 0;
}
```

Dalam kode tersebut diletakkan pada xmp_readdir() karena diinginkan untuk memperlihatkan bagaimana kondisi file ketika dibaca untuk menentukan apakah file akan dimodularisasi atau tidak. Saat dimodularisasi akan dijalankan fungsi encrypt() sedangkan non modular akan dijalankan fungsi decode().

#### File Log

Untuk mengerjakan bagian log maka yang perlu dilakukan adalah menuliskan ke log sesuai dengen fungsi fuse yang diinginkan. Misalkan untuk rename maka pada bagian xmp_rename akan ditambahkan logsystem() yang gunanya untuk menulis log setiap fungsi dipanggil.  

```c
void logSystem(char* c, int type){
    FILE * logFile = fopen("/home/rayhanalmer/fs_module.log", "a");
	time_t currTime;
	struct tm * time_info;
	time ( &currTime );
	time_info = localtime (&currTime);
    int yr=time_info->tm_year,mon=time_info->tm_mon,day=time_info->tm_mday,hour=time_info->tm_hour,min=time_info->tm_min,sec=time_info->tm_sec;
    if(type==1){//report type
        fprintf(logFile, "REPORT::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
    }
    else if(type==2){ //flag type
        fprintf(logFile, "FLAG::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
    }
    fclose(logFile);
}
```

- void logSystem(char* c, int type): Fungsi ini menerima dua parameter, yaitu c yang merupakan string yang akan dicatat dalam log, dan type yang menentukan jenis catatan yang akan dibuat.  

- FILE * logFile = fopen("/home/valdo/fs_module.log", "a"): Fungsi fopen digunakan untuk membuka file fs_module.log dalam mode "a" (append). Hal ini berarti file akan dibuka untuk penulisan pada akhir file tanpa menghapus konten yang sudah ada. File tersebut akan diwakili oleh pointer logFile.  

- time_t currTime; struct tm * time_info; time ( &currTime ); time_info = localtime (&currTime);: Variabel currTime bertipe time_t digunakan untuk menyimpan waktu saat ini dalam bentuk UNIX timestamp. Selanjutnya, variabel time_info bertipe struct tm* digunakan untuk menyimpan informasi waktu yang diuraikan, yaitu tahun, bulan, tanggal, jam, menit, dan detik. Fungsi time dan localtime digunakan untuk mendapatkan waktu lokal saat ini.  

- int yr=time_info->tm_year,mon=time_info->tm_mon,day=time_info->tm_mday,hour=time_info->tm_hour,min=time_info->tm_min,sec=time_info->tm_sec;: Mengambil informasi waktu yang telah diuraikan dari time_info dan menyimpannya dalam variabel terpisah. Variabel yr menyimpan tahun, mon menyimpan bulan, day menyimpan tanggal, hour menyimpan jam, min menyimpan menit, dan sec menyimpan detik.  

- if(type==1): Melakukan pengecekan tipe catatan yang akan dibuat. Jika type bernilai 1, maka ini merupakan catatan jenis "report".  

- fprintf(logFile, "REPORT::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c): Menggunakan fungsi fprintf untuk menulis catatan ke file fs_module.log. Catatan ini memiliki format yang ditentukan, yaitu "REPORT::YYYYMMDD-HH:MM:SS::". Format tanggal dan waktu diisi dengan nilai-nilai yang telah diambil sebelumnya, dan  diisi dengan string c.  

- else if(type==2): Jika type bernilai 2, maka ini merupakan catatan jenis "flag".  

- fprintf(logFile, "FLAG::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c): Sama seperti sebelumnya, menggunakan fprintf untuk menulis catatan ke file fs_module.log. Catatan ini memiliki format "FLAG::YYYYMMDD-HH:MM:SS::", dengan format tanggal, waktu, dan  diisi dengan nilai-nilai yang sesuai.  

- fclose(logFile): Setelah selesai menulis catatan, file fs_module.log ditutup menggunakan fclose untuk mengosongkan buffer dan memastikan catatan tersimpan dengan baik.  

#### Modularisasi Konten

Modularisasi konten dilakukan dalam fungsi encrypt sebagai berikut:

```c
void encrypt(char* enc1, char * enc2){
    if(strcmp(enc1, ".") == 0) return;
    if(strcmp(enc1, "..") == 0)return;
    
    int chunks=0, i, accum;

    char largeFileName[200];    //change to your path
    sprintf(largeFileName,"%s/%s/%s",dirPath,enc2,enc1);
    printf("%s\n",largeFileName);

    char filename[260];//base name for small files.
    sprintf(filename,"%s.",largeFileName);
    //printf("%s\n",filename);

    char smallFileName[300];
    char line[1080];
    FILE *fp1, *fp2;
    long sizeFile = file_size(largeFileName);
    // printf("File size : %ld\n",sizeFile);
    
	if (sizeFile > 1025){
		chunks = sizeFile/CHUNK + 1;//ensure end of file
		// printf("%d",chunks);
		fp1 = fopen(largeFileName, "r");
		if(fp1)
		{
			char number[5];
			printf("Splitting\n");
			for(i=0;i<chunks;i++)
			{
				accum = 0;
				sprintf(number,"%03d",i+1);
				
				sprintf(smallFileName, "%s%s", filename, number);

				// Check if segment file already exists, skip if it does
				if (segment_exists(smallFileName)) {
					continue;
				}
				
				fp2 = fopen(smallFileName, "wb");
				if (fp2) {
					while (accum < CHUNK && !feof(fp1)) {
						size_t bytesRead = fread(line, 1, sizeof(line), fp1);
						size_t bytesToWrite = bytesRead;
						if (accum + bytesToWrite > CHUNK) {
							bytesToWrite = CHUNK - accum;
						}
						fwrite(line, 1, bytesToWrite, fp2);
						accum += bytesToWrite;
					}
					fclose(fp2);
				}
			}
			fclose(fp1);
			// Hapus file asli setelah pemecahan selesai
			if (remove(largeFileName) != 0) {
				printf("Gagal menghapus file asli: %s\n", largeFileName);
			} else {
				printf("File asli dihapus: %s\n", largeFileName);
			}
		}
	}
    printf("keluar\n");
}
```

- int chunks=0, i, accum;: Variabel chunks digunakan untuk menyimpan jumlah file kecil yang akan dihasilkan setelah pemecahan file besar. Variabel i digunakan sebagai iterator dalam loop, dan variabel accum digunakan untuk melacak jumlah byte yang telah ditulis ke file kecil saat melakukan pemecahan.  

- char largeFileName[200]; sprintf(largeFileName,"%s/%s/%s",dirPath,enc2,enc1);: Membentuk path lengkap dari file besar dengan menggabungkan dirPath, enc2, dan enc1. Path ini akan digunakan untuk membaca file besar yang akan dipotong.  

- char filename[260]; sprintf(filename,"%s.",largeFileName);: Membentuk base name untuk file-file kecil yang akan dihasilkan. Base name ini merupakan gabungan dari largeFileName dan tanda ".".  

- char smallFileName[300]; char line[1080]; FILE *fp1, *fp2;: Variabel smallFileName digunakan untuk menyimpan nama file kecil yang akan dihasilkan dalam setiap iterasi. Variabel line digunakan untuk membaca dan menulis data dari file besar ke file kecil. Variabel fp1 dan fp2 adalah pointer yang akan digunakan untuk membuka file besar dan file kecil, masing-masing.  

- long sizeFile = file_size(largeFileName);: Menghitung ukuran file besar dengan memanggil fungsi file_size. Hasilnya disimpan dalam variabel sizeFile.  

- if (sizeFile > 1025){: Memeriksa apakah ukuran file besar melebihi batas minimum yang ditentukan (1025 byte). Jika ukuran file kurang dari atau sama dengan batas tersebut, maka pemecahan tidak perlu dilakukan. 

- chunks = sizeFile/CHUNK + 1;: Menghitung jumlah file kecil yang akan dihasilkan berdasarkan ukuran file besar. Pembagian dilakukan dengan ukuran tetap yang ditentukan oleh konstanta CHUNK. Penambahan 1 dilakukan untuk memastikan bahwa seluruh data file besar tercakup dalam file-file kecil yang dihasilkan.  

- fp1 = fopen(largeFileName, "r");: Membuka file besar dalam mode "r" (read). File ini akan dibaca untuk memecah datanya menjadi file-file kecil.  

- char number[5]; sprintf(number,"%03d",i+1);: Membentuk angka yang akan digunakan dalam nama file kecil. Angka ini berupa nomor urut dengan tiga digit, dimulai dari 001.  

- sprintf(smallFileName, "%s%s", filename, number);: Membentuk nama file kecil dengan menggabungkan base name filename dan angka number.  

- if (segment_exists(smallFileName)) { continue; }: Memeriksa apakah file kecil dengan nama yang sama telah ada. Jika sudah ada, iterasi selanjutnya akan dilanjutkan dan file kecil baru tidak akan dibuat. Ini menghindari duplikasi file saat fungsi encrypt dipanggil ulang untuk file yang sama.  

- fp2 = fopen(smallFileName, "wb");: Membuka file kecil dalam mode "wb" (write binary). File ini akan digunakan untuk menulis data yang dipotong dari file besar.  

- while (accum < CHUNK && !feof(fp1)) { ... }: Melakukan loop untuk membaca data dari file besar dan menulisnya ke file kecil. Loop akan berlanjut selama accum (jumlah byte yang telah ditulis ke file kecil) belum mencapai ukuran tetap CHUNK dan belum mencapai akhir file besar.  

- fwrite(line, 1, bytesToWrite, fp2);: Menulis bytesToWrite byte data ke file kecil dari buffer line. bytesToWrite adalah jumlah byte yang akan ditulis dalam satu iterasi.  

- fclose(fp2);: Menutup file kecil setelah selesai menulis data.  

- Setelah loop selesai, fclose(fp1); akan menutup file besar.  

- if (remove(largeFileName) != 0) { ... }: Menghapus file asli setelah pemecahan selesai. Jika penghapusan gagal, pesan kesalahan akan ditampilkan. Jika berhasil, pesan berhasil dihapus akan ditampilkan.  

#### Mengembalikan Konten

Mengembalikan konten(fixed) dilakukan dengan fungsi decode sebagai berikut:

```c
void decode(const char *directoryPath, const char *prefix) {
    DIR *dir;
    struct dirent *entry;
    char filePath[1000];
    char mergedFilePath[1000];
    FILE *mergedFile;
    size_t prefixLength = strlen(prefix);

    dir = opendir(directoryPath);

    sprintf(mergedFilePath, "%s/%s", directoryPath, prefix);
    mergedFile = fopen(mergedFilePath, "w");

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strncmp(entry->d_name, prefix, prefixLength) == 0) {
            sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
            FILE *file = fopen(filePath, "r");

			// Skip the merged file itself
            if (strcmp(entry->d_name, prefix) == 0) {
                fclose(file);
                continue;
            }

            char buffer[1024];
            size_t bytesRead;

            while ((bytesRead = fread(buffer, 1, sizeof(buffer), file)) > 0) {
                fwrite(buffer, 1, bytesRead, mergedFile);
            }

            fclose(file);
        } 

    }

    closedir(dir);
    fclose(mergedFile);

    dir = opendir(directoryPath);

	//remove useless file
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            if (strncmp(entry->d_name, prefix, prefixLength) == 0) {
                char extension[1000];
                sprintf(extension, ".%03d", atoi(entry->d_name + prefixLength + 1));
                if (strstr(entry->d_name, extension) != NULL) {
                    sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
                    if (strcmp(filePath, mergedFilePath) != 0) {
                        remove(filePath);
                    }
                }
            }
        }
    }

    closedir(dir);
}
```

- DIR *dir; struct dirent *entry; char filePath[1000]; char mergedFilePath[1000]; FILE *mergedFile; size_t prefixLength = strlen(prefix);: Variabel dir digunakan untuk menyimpan pointer ke direktori yang akan diproses. Variabel entry digunakan untuk membaca setiap entri di dalam direktori. Variabel filePath dan mergedFilePath digunakan untuk menyimpan path lengkap dari file-file kecil dan file besar yang akan digabungkan. Variabel mergedFile adalah pointer yang akan digunakan untuk membuka file besar dalam mode "w" (write). Variabel prefixLength menyimpan panjang dari prefix yang digunakan untuk mencocokkan nama file.  

- dir = opendir(directoryPath);: Membuka direktori yang diberikan dalam parameter directoryPath dengan menggunakan opendir.  

- sprintf(mergedFilePath, "%s/%s", directoryPath, prefix); mergedFile = fopen(mergedFilePath, "w");: Membentuk path lengkap untuk file besar dengan menggabungkan directoryPath dan prefix. Path ini akan digunakan untuk membuka file besar yang akan digabungkan.  

- while ((entry = readdir(dir)) != NULL) { ... }: Melakukan loop untuk membaca setiap entri di dalam direktori.  

- if (entry->d_type == DT_REG && strncmp(entry->d_name, prefix, prefixLength) == 0) { ... }: Memeriksa apakah entri saat ini adalah file (DT_REG) dan nama file tersebut memiliki prefix yang sesuai dengan prefix. Jika iya, maka file tersebut adalah file kecil yang akan digabungkan.  

- sprintf(filePath, "%s/%s", directoryPath, entry->d_name); FILE *file = fopen(filePath, "r");: Membentuk path lengkap untuk file kecil saat ini dengan menggabungkan directoryPath dan nama file kecil yang sedang diproses. File ini akan dibuka dalam mode "r" (read) untuk membaca isinya.  

- if (strcmp(entry->d_name, prefix) == 0) { fclose(file); continue; }: Memeriksa apakah file yang sedang diproses adalah file besar yang sama dengan prefix. Jika iya, maka file tersebut adalah file gabungan itu sendiri dan tidak perlu digabungkan. File tersebut akan ditutup dan loop akan melanjutkan ke entri berikutnya.  

- char buffer[1024]; size_t bytesRead; while ((bytesRead = fread(buffer, 1, sizeof(buffer), file)) > 0) { fwrite(buffer, 1, bytesRead, mergedFile); }: Membaca data dari file kecil dan menuliskannya ke file besar. Data akan dibaca dalam buffer sebesar 1024 byte (buffer) dan ditulis ke file besar menggunakan fwrite.  

- Setelah loop selesai, file kecil saat ini (file) akan ditutup.  

- closedir(dir); fclose(mergedFile);: Menutup direktori dan file besar setelah selesai.  

- dir = opendir(directoryPath);: Membuka direktori kembali untuk memproses entri berikutnya.  

- while ((entry = readdir(dir)) != NULL) { ... }: Melakukan loop lagi untuk membaca entri di dalam direktori.  

- if (entry->d_type == DT_REG) { if (strncmp(entry->d_name, prefix, prefixLength) == 0) { ... } }: Memeriksa apakah entri saat ini adalah file (DT_REG) dan memiliki prefix yang sesuai dengan prefix. Jika iya, maka file tersebut adalah file kecil yang tidak akan digabungkan.  

- sprintf(extension, ".%03d", atoi(entry->d_name + prefixLength + 1));: Membentuk ekstensi file kecil dengan memotong angka setelah prefix dan memformatnya sebagai tiga digit dengan "%03d". Ekstensi ini digunakan untuk memeriksa apakah file kecil tersebut adalah file yang telah digabungkan sebelumnya.  

- if (strstr(entry->d_name, extension) != NULL) { ... }: Memeriksa apakah ekstensi file saat ini (entry->d_name) mengandung ekstensi yang sama dengan extension. Jika iya, maka file tersebut adalah file kecil yang telah digabungkan sebelumnya dan perlu dihapus.  

- sprintf(filePath, "%s/%s", directoryPath, entry->d_name); if (strcmp(filePath, mergedFilePath) != 0) { remove(filePath); }: Membentuk path lengkap untuk file kecil saat ini dan memeriksa apakah path tersebut sama dengan path file besar (mergedFilePath). Jika tidak sama, maka file kecil tersebut dihapus menggunakan remove.  

- Setelah loop selesai, direktori akan ditutup kembali dengan closedir(dir).  

### Output

## SOAL 5

a. Program rahasia.c merupakan file FUSE yang akan digunakan untuk melakukan mount folder pada Docker Container. **Unduh file rahasia.zip kemudian lakukan unzip pada file rahasia.zip menjadi folder rahasia.**

Mengunduh file rahasia.zip dari sumber yang telah disediakan. Dan menyimpannya di direktori

![direktori_1](Soal 5/direktori_1.png)

Meng-unzip file rahasia menjadi folder rahasia

![unzip](Soal 5/unzip.png)

b. Seperti soal a, folder rahasia akan di-mount pada Docker Container dengan image bernama rahasia_di_docker_<Kode Kelompok> pada direktori /usr/share. Gunakan Ubuntu Focal Fossa sebagai base image pada Dockerfile

Membuat Dockerfile dengan base image ubuntu local fassa
![base_image_ubuntu_focal_fassa](Soal 5/base_image_ubuntu_focal_fassa.png)

Membangun Image 
![membuat_image](Soal 5/membuat_image.png)

Melakukan mount folder ke docker container pada direktori usr/share
![mount_folder](Soal 5/mount_folder.png)

Folder telah ada di docker container pada direktori usr/share
![ada_di_direktori](Soal 5/ada_di_direktori.png)

c. Setelah melakukan mount, buatlah register system yang menyimpan kredensial berupa username dan password. Agar lebih aman, password disimpan dengan menggunakan hashing MD5. Untuk mempermudah kalian, gunakan system()  serta gunakan built-in program untuk melakukan hashing MD5 pada Linux (tidak wajib). Username dan password akan disimpan dengan format <username>;<password>. Tidak boleh ada user yang melakukan register dengan username yang sama. Kemudian, Buatlah login system yang mencocokkan kredensial antara username dan password.


- mendefinisi struktur User yang berisi dua string: username dan password. struktur digunakan untuk menyimpan informasi pengguna yang terdaftar.
`struct User {
    char username[MAX_USERNAME_LENGTH];
    char password[MAX_PASSWORD_LENGTH];
};
`

- array users yang merupakan kumpulan data pengguna. userCount digunakan untuk melacak jumlah pengguna yang telah terdaftar.
`struct User users[MAX_USERS];
int userCount = 0;
`

- membuat fungsi registerUser() yang digunakan untuk mendaftarkan pengguna baru. Variabel username dan password digunakan untuk menyimpan input pengguna. md5Password digunakan untuk menyimpan hasil hashing MD5 dari password.
`void registerUser() {
    char username[MAX_USERNAME_LENGTH];
    char password[MAX_PASSWORD_LENGTH];
    char md5Password[MD5_DIGEST_LENGTH * 2 + 1];
`

- meminta pengguna untuk memasukkan nama pengguna dan menyimpannya dalam variabel username.
`printf("Masukkan username: ");
scanf("%s", username);
`

- memeriksa apakah username yang dimasukkan sudah terdaftar sebelumnya. Jika sudah terdaftar, program akan mencetak pesan kesalahan dan menghentikan proses pendaftaran.
`for (int i = 0; i < userCount; i++) {
    if (strcmp(users[i].username, username) == 0) {
        printf("Username sudah terdaftar. Silakan coba dengan username lain.\n");
        return;
    }
}
`

- Program akan meminta pengguna untuk memasukkan kata sandi dan menyimpannya dalam variabel password.
`printf("Masukkan password: ");
scanf("%s", password);
`

-  password akan di-hash menggunakan fungsi MD5 dari pustaka OpenSSL. Hasil hash akan disimpan dalam array hash.
 `unsigned char hash[MD5_DIGEST_LENGTH];
MD5((unsigned char*)password, strlen(password), hash);
`

- Hasil hash yang berupa array byte akan dikonversi menjadi string heksadesimal dan disimpan dalam variabel md5Password.
`for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
    sprintf(&md5Password[i * 2], "%02x", (unsigned int)hash[i]);
}
`

- Data pengguna baru, yaitu nama pengguna dan password yang sudah di-hash, akan disimpan dalam array users. userCount akan ditingkatkan untuk mengindikasikan bahwa pengguna baru telah terdaftar. Pesan registrasi berhasil akan dicetak ke layar.
`userCount++;
printf("Registrasi berhasil!\n");
`

- membuat fungsi loginUser() yang digunakan untuk melakukan proses login. Variabel username dan password digunakan untuk menyimpan input pengguna. md5Password digunakan untuk menyimpan hasil hashing MD5 dari password.
`void loginUser() {
    char username[MAX_USERNAME_LENGTH];
    char password[MAX_PASSWORD_LENGTH];
    char md5Password[MD5_DIGEST_LENGTH * 2 + 1];
`

- Program akan meminta pengguna untuk memasukkan nama pengguna dan password, kemudian menyimpannya dalam variabel username dan password.
`printf("Masukkan username: ");
scanf("%s", username);

printf("Masukkan password: ");
scanf("%s", password);
`

- Password yang dimasukkan akan di-hash menggunakan fungsi MD5 dari pustaka OpenSSL. Hasil hash akan disimpan dalam array hash
`unsigned char hash[MD5_DIGEST_LENGTH];
MD5((unsigned char*)password, strlen(password), hash);
`
- Hasil hash yang berupa array byte akan dikonversi menjadi string heksadesimal dan disimpan dalam variabel md5Password.
`for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
    sprintf(&md5Password[i * 2], "%02x", (unsigned int)hash[i]);
}
`
- memeriksa apakah terdapat kredensial yang cocok dengan username dan password yang dimasukkan. Jika cocok, program akan mencetak pesan login berhasil. Jika tidak ada kredensial yang cocok, program akan mencetak pesan login gagal.
`for (int i = 0; i < userCount; i++) {
    if (strcmp(users[i].username, username) == 0 && strcmp(users[i].password, md5Password) == 0) {
        printf("Login berhasil!\n");
        return;
    }
}
`
- Membuat fungsi main() adalah tempat program dijalankan. Di dalamnya, terdapat loop yang memungkinkan pengguna memilih opsi menu: Register, Login, atau Exit. Pilihan pengguna akan ditangani oleh switch case yang memanggil fungsi registerUser() atau loginUser() sesuai dengan pilihan yang dipilih. Program akan terus berjalan hingga pengguna memilih opsi Exit.
`int main() {
    int choice;
    while (1) {
        printf("Menu:\n");
        printf("1. Register\n");
        printf("2. Login\n");
        printf("3. Exit\n");
        printf("Pilihan Anda: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                registerUser();
                break;
            case 2:
                loginUser();
                break;
            case 3:
                exit(0);
            default:
                printf("Pilihan tidak valid.\n");
        }
    }
    return 0;
}
`

- KENDALA:
masih gagal dalam membuka kresidensial
![buka_kresidensial](Soal 5/buka_kresidensial.png)

Sebab: folder rahasia.c belum dapat diakses dalam docker container



